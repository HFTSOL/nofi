/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { NOFI_TYPES } from './nofi';
import * as i0 from "@angular/core";
export class NofiService {
    constructor() {
        this._notifier = null;
    }
    /**
     * @param {?} notification
     * @return {?}
     */
    notify(notification) {
        if (this._notifier) {
            this._notifier.next(notification);
        }
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    success(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Success,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    info(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Info,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    warning(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Warning,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    failure(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Failure,
            title,
            message,
            duration
        });
    }
    /**
     * @return {?}
     */
    register() {
        this._notifier = new Subject();
        return this._notifier;
    }
}
NofiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NofiService.ctorParameters = () => [];
/** @nocollapse */ NofiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NofiService_Factory() { return new NofiService(); }, token: NofiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NofiService.prototype._notifier;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbm9maS8iLCJzb3VyY2VzIjpbImxpYi9ub2ZpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFhLE9BQU8sRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUN6QyxPQUFPLEVBQVEsVUFBVSxFQUFDLE1BQU0sUUFBUSxDQUFDOztBQUt6QyxNQUFNLE9BQU8sV0FBVztJQUd0QjtRQUZRLGNBQVMsR0FBbUIsSUFBSSxDQUFDO0lBR3pDLENBQUM7Ozs7O0lBRU0sTUFBTSxDQUFDLFlBQW1CO1FBQy9CLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNuQztJQUNILENBQUM7Ozs7Ozs7SUFFTSxPQUFPLENBQUMsS0FBYSxFQUFFLE9BQWUsRUFBRSxRQUFRLEdBQUcsS0FBSztRQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ1YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPO1lBQ3hCLEtBQUs7WUFDTCxPQUFPO1lBQ1AsUUFBUTtTQUNULENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFTSxJQUFJLENBQUMsS0FBYSxFQUFFLE9BQWUsRUFBRSxRQUFRLEdBQUcsS0FBSztRQUMxRCxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ1YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFJO1lBQ3JCLEtBQUs7WUFDTCxPQUFPO1lBQ1AsUUFBUTtTQUNULENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFTSxPQUFPLENBQUMsS0FBYSxFQUFFLE9BQWUsRUFBRSxRQUFRLEdBQUcsS0FBSztRQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ1YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPO1lBQ3hCLEtBQUs7WUFDTCxPQUFPO1lBQ1AsUUFBUTtTQUNULENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFTSxPQUFPLENBQUMsS0FBYSxFQUFFLE9BQWUsRUFBRSxRQUFRLEdBQUcsS0FBSztRQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ1YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPO1lBQ3hCLEtBQUs7WUFDTCxPQUFPO1lBQ1AsUUFBUTtTQUNULENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFTSxRQUFRO1FBQ2IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDOzs7WUF0REYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7Ozs7O0lBRUMsZ0NBQXlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7T2JzZXJ2YWJsZSwgU3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0lOb2ZpLCBOT0ZJX1RZUEVTfSBmcm9tICcuL25vZmknO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBOb2ZpU2VydmljZSB7XG4gIHByaXZhdGUgX25vdGlmaWVyOiBTdWJqZWN0PElOb2ZpPiA9IG51bGw7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBwdWJsaWMgbm90aWZ5KG5vdGlmaWNhdGlvbjogSU5vZmkpIHtcbiAgICBpZiAodGhpcy5fbm90aWZpZXIpIHtcbiAgICAgIHRoaXMuX25vdGlmaWVyLm5leHQobm90aWZpY2F0aW9uKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc3VjY2Vzcyh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIGR1cmF0aW9uID0gMjUwMDApIHtcbiAgICB0aGlzLm5vdGlmeSh7XG4gICAgICB0eXBlOiBOT0ZJX1RZUEVTLlN1Y2Nlc3MsXG4gICAgICB0aXRsZSxcbiAgICAgIG1lc3NhZ2UsXG4gICAgICBkdXJhdGlvblxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGluZm8odGl0bGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nLCBkdXJhdGlvbiA9IDI1MDAwKSB7XG4gICAgdGhpcy5ub3RpZnkoe1xuICAgICAgdHlwZTogTk9GSV9UWVBFUy5JbmZvLFxuICAgICAgdGl0bGUsXG4gICAgICBtZXNzYWdlLFxuICAgICAgZHVyYXRpb25cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyB3YXJuaW5nKHRpdGxlOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZywgZHVyYXRpb24gPSAyNTAwMCkge1xuICAgIHRoaXMubm90aWZ5KHtcbiAgICAgIHR5cGU6IE5PRklfVFlQRVMuV2FybmluZyxcbiAgICAgIHRpdGxlLFxuICAgICAgbWVzc2FnZSxcbiAgICAgIGR1cmF0aW9uXG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgZmFpbHVyZSh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIGR1cmF0aW9uID0gMjUwMDApIHtcbiAgICB0aGlzLm5vdGlmeSh7XG4gICAgICB0eXBlOiBOT0ZJX1RZUEVTLkZhaWx1cmUsXG4gICAgICB0aXRsZSxcbiAgICAgIG1lc3NhZ2UsXG4gICAgICBkdXJhdGlvblxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHJlZ2lzdGVyKCk6IE9ic2VydmFibGU8SU5vZmk+IHtcbiAgICB0aGlzLl9ub3RpZmllciA9IG5ldyBTdWJqZWN0PElOb2ZpPigpO1xuICAgIHJldHVybiB0aGlzLl9ub3RpZmllcjtcbiAgfVxufVxuIl19