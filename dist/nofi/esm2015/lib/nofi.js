/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Nofi
/**
 * @record
 */
export function INofi() { }
if (false) {
    /** @type {?|undefined} */
    INofi.prototype.id;
    /** @type {?} */
    INofi.prototype.title;
    /** @type {?} */
    INofi.prototype.message;
    /** @type {?|undefined} */
    INofi.prototype.duration;
    /** @type {?|undefined} */
    INofi.prototype.isSticky;
    /** @type {?|undefined} */
    INofi.prototype.isNavSticky;
    /** @type {?|undefined} */
    INofi.prototype.isHtml;
    /** @type {?|undefined} */
    INofi.prototype.buttons;
    /** @type {?|undefined} */
    INofi.prototype.callback;
    /** @type {?} */
    INofi.prototype.type;
}
/**
 * @record
 */
export function INofiButton() { }
if (false) {
    /** @type {?|undefined} */
    INofiButton.prototype.classes;
    /** @type {?|undefined} */
    INofiButton.prototype.altText;
    /** @type {?} */
    INofiButton.prototype.text;
    /** @type {?|undefined} */
    INofiButton.prototype.callback;
}
/** @enum {number} */
const NOFI_TYPES = {
    Vanilla: 0,
    Success: 1,
    Info: 2,
    Warning: 3,
    Failure: 4,
};
export { NOFI_TYPES };
NOFI_TYPES[NOFI_TYPES.Vanilla] = 'Vanilla';
NOFI_TYPES[NOFI_TYPES.Success] = 'Success';
NOFI_TYPES[NOFI_TYPES.Info] = 'Info';
NOFI_TYPES[NOFI_TYPES.Warning] = 'Warning';
NOFI_TYPES[NOFI_TYPES.Failure] = 'Failure';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25vZmkvIiwic291cmNlcyI6WyJsaWIvbm9maS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUVBLDJCQVdDOzs7SUFWQyxtQkFBWTs7SUFDWixzQkFBYzs7SUFDZCx3QkFBZ0I7O0lBQ2hCLHlCQUFrQjs7SUFDbEIseUJBQW1COztJQUNuQiw0QkFBc0I7O0lBQ3RCLHVCQUFpQjs7SUFDakIsd0JBQWdCOztJQUNoQix5QkFBZTs7SUFDZixxQkFBaUI7Ozs7O0FBR25CLGlDQUtDOzs7SUFKQyw4QkFBaUI7O0lBQ2pCLDhCQUFpQjs7SUFDakIsMkJBQWE7O0lBQ2IsK0JBQWU7Ozs7SUFLZixVQUFXO0lBQ1gsVUFBVztJQUNYLE9BQVE7SUFDUixVQUFXO0lBQ1gsVUFBVyIsInNvdXJjZXNDb250ZW50IjpbIi8vIE5vZmlcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSU5vZmkge1xyXG4gIGlkPzogc3RyaW5nO1xyXG4gIHRpdGxlOiBzdHJpbmc7XHJcbiAgbWVzc2FnZTogc3RyaW5nO1xyXG4gIGR1cmF0aW9uPzogbnVtYmVyO1xyXG4gIGlzU3RpY2t5PzogYm9vbGVhbjtcclxuICBpc05hdlN0aWNreT86IGJvb2xlYW47XHJcbiAgaXNIdG1sPzogYm9vbGVhbjtcclxuICBidXR0b25zPzogYW55W107XHJcbiAgY2FsbGJhY2s/OiBhbnk7XHJcbiAgdHlwZTogTk9GSV9UWVBFUztcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJTm9maUJ1dHRvbiB7XHJcbiAgY2xhc3Nlcz86IHN0cmluZztcclxuICBhbHRUZXh0Pzogc3RyaW5nO1xyXG4gIHRleHQ6IHN0cmluZztcclxuICBjYWxsYmFjaz86IGFueTtcclxufVxyXG5cclxuXHJcbmV4cG9ydCBlbnVtIE5PRklfVFlQRVMge1xyXG4gIFZhbmlsbGEgPSAwLFxyXG4gIFN1Y2Nlc3MgPSAxLFxyXG4gIEluZm8gPSAyLFxyXG4gIFdhcm5pbmcgPSAzLFxyXG4gIEZhaWx1cmUgPSA0XHJcbn1cclxuIl19