/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { NOFI_TYPES } from '../nofi';
export class NotifierTypePipe {
    /**
     * @param {?} nofi
     * @return {?}
     */
    transform(nofi) {
        switch (nofi.type) {
            case NOFI_TYPES.Vanilla:
                return '';
            case NOFI_TYPES.Success:
                return 'success';
            case NOFI_TYPES.Info:
                return 'info';
            case NOFI_TYPES.Warning:
                return 'warning';
            case NOFI_TYPES.Failure:
                return 'failure';
        }
    }
}
NotifierTypePipe.decorators = [
    { type: Pipe, args: [{ name: 'notifierType' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZS1jbGFzcy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbm9maS8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy90eXBlLWNsYXNzLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxJQUFJLEVBQWdCLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBUSxVQUFVLEVBQUMsTUFBTSxTQUFTLENBQUM7QUFHMUMsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7SUFDM0IsU0FBUyxDQUFDLElBQVc7UUFDbkIsUUFBTyxJQUFJLENBQUMsSUFBSSxFQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUMsT0FBTztnQkFDckIsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNyQixPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLFVBQVUsQ0FBQyxJQUFJO2dCQUNsQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNyQixPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNyQixPQUFPLFNBQVMsQ0FBQztTQUNwQjtJQUNILENBQUM7OztZQW5CRixJQUFJLFNBQUMsRUFBQyxJQUFJLEVBQUUsY0FBYyxFQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtQaXBlLCBQaXBlVHJhbnNmb3JtfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtJTm9maSwgTk9GSV9UWVBFU30gZnJvbSAnLi4vbm9maSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ25vdGlmaWVyVHlwZSd9KVxyXG5leHBvcnQgY2xhc3MgTm90aWZpZXJUeXBlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gIHRyYW5zZm9ybShub2ZpOiBJTm9maSk6IHN0cmluZyB7XHJcbiAgICBzd2l0Y2gobm9maS50eXBlKXtcclxuICAgICAgY2FzZSBOT0ZJX1RZUEVTLlZhbmlsbGE6XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG5cclxuICAgICAgY2FzZSBOT0ZJX1RZUEVTLlN1Y2Nlc3M6XHJcbiAgICAgICAgcmV0dXJuICdzdWNjZXNzJztcclxuXHJcbiAgICAgIGNhc2UgTk9GSV9UWVBFUy5JbmZvOlxyXG4gICAgICAgIHJldHVybiAnaW5mbyc7XHJcblxyXG4gICAgICBjYXNlIE5PRklfVFlQRVMuV2FybmluZzpcclxuICAgICAgICByZXR1cm4gJ3dhcm5pbmcnO1xyXG5cclxuICAgICAgY2FzZSBOT0ZJX1RZUEVTLkZhaWx1cmU6XHJcbiAgICAgICAgcmV0dXJuICdmYWlsdXJlJztcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19