/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NofiComponent } from './nofi.component';
import { NotifierTypePipe } from './pipes/type-class.pipe';
export class NofiModule {
}
NofiModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    NofiComponent,
                    NotifierTypePipe
                ],
                imports: [
                    CommonModule,
                    FontAwesomeModule,
                ],
                exports: [NofiComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ub2ZpLyIsInNvdXJjZXMiOlsibGliL25vZmkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxrQ0FBa0MsQ0FBQztBQUNuRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFjekQsTUFBTSxPQUFPLFVBQVU7OztZQVh0QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLGFBQWE7b0JBQ2IsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixpQkFBaUI7aUJBQ2xCO2dCQUNELE9BQU8sRUFBRSxDQUFDLGFBQWEsQ0FBQzthQUN6QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0ZvbnRBd2Vzb21lTW9kdWxlfSBmcm9tICdAZm9ydGF3ZXNvbWUvYW5ndWxhci1mb250YXdlc29tZSc7XG5pbXBvcnQge05vZmlDb21wb25lbnR9IGZyb20gJy4vbm9maS5jb21wb25lbnQnO1xuaW1wb3J0IHtOb3RpZmllclR5cGVQaXBlfSBmcm9tICcuL3BpcGVzL3R5cGUtY2xhc3MucGlwZSc7XG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgTm9maUNvbXBvbmVudCxcbiAgICBOb3RpZmllclR5cGVQaXBlXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9udEF3ZXNvbWVNb2R1bGUsXG4gIF0sXG4gIGV4cG9ydHM6IFtOb2ZpQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBOb2ZpTW9kdWxlIHtcbn1cbiJdfQ==