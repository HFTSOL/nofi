/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Guid } from 'guid-typescript';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { NOFI_TYPES } from './nofi';
export class NofiComponent {
    constructor() {
        this.NOFI_TYPES = NOFI_TYPES;
        // Icons
        this.faTimesCircle = faTimesCircle;
        this._notifications = [];
        this._onNotification = (/**
         * @param {?} nofi
         * @return {?}
         */
        (nofi) => {
            nofi.id = Guid.create().toString();
            if (nofi.duration && !isNaN(nofi.duration))
                setTimeout(this._timeoutNotification.bind(this, nofi.id), nofi.duration);
            this._notifications.push(nofi);
        });
    }
    /**
     * @return {?}
     */
    get notifications() {
        return this._notifications;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} nofiId
     * @return {?}
     */
    onCloseClick(nofiId) {
        this._timeoutNotification(nofiId);
    }
    /**
     * @param {?} nofi
     * @param {?} nofiBtn
     * @return {?}
     */
    onNofiBtnClick(nofi, nofiBtn) {
        if (nofi) {
            if (nofiBtn.callback && typeof nofiBtn.callback === 'function')
                nofiBtn.callback();
            else if (nofi.callback && typeof nofi.callback === 'function')
                nofi.callback();
            this._timeoutNotification(nofi.id);
        }
    }
    /**
     * @private
     * @param {?} nofiId
     * @return {?}
     */
    _timeoutNotification(nofiId) {
        // this._logger.debug(`${this.CLASSNAME} > _timeoutNotification() > nofiId: ${nofiId}`);
        if (!nofiId)
            return;
        /** @type {?} */
        const index = this._notifications.findIndex((/**
         * @param {?} n
         * @return {?}
         */
        (n) => {
            return n.id === nofiId;
        }));
        if (index >= 0)
            this._notifications.splice(index, 1);
    }
}
NofiComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-nofi',
                template: "\r\n<div class=\"nofis\">\r\n  <ul>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"!nofi.buttons || !nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <fa-icon class=\"nofi-x\" [icon]=\"faTimesCircle\" (click)=\"onCloseClick(nofi.id)\"></fa-icon>\r\n      </div>\r\n    </li>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"nofi.buttons && nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <div class=\"nofi-buttons\">\r\n          <button *ngFor=\"let nofiBtn of nofi.buttons\"\r\n                  [class]=\"nofiBtn.classes || ''\"\r\n                  [title]=\"nofiBtn.altText || ''\"\r\n                  (click)=\"onNofiBtnClick(nofi, nofiBtn)\">\r\n            {{nofiBtn.text}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>\r\n",
                styles: [".nofis{position:fixed;right:10px;top:50px;width:250px;z-index:1000}.nofis>ul>li{margin-bottom:10px}.nofis .nofi{background-color:#030303;border:1px solid #fff;padding:10px;position:relative;border-radius:12px;box-shadow:10px 10px 20px 0 rgba(0,0,0,.75)}.nofis .nofi.success{background-color:#51a351}.nofis .nofi.info{background-color:#2f96b4}.nofis .nofi.warning{background-color:#f89406}.nofis .nofi.failure{background-color:#bd362f}.nofis .nofi-title{color:#fff;font-weight:600;font-size:14px}.nofis .nofi-msg{color:#fff;font-size:12px;margin-bottom:5px}.nofis .nofi-x{color:#fff;font-size:16px;position:absolute;right:10px;top:10px;z-index:1010}.nofis .nofi-buttons{text-align:center;width:100%}.nofis .nofi-buttons>button{margin-bottom:3px;margin-right:10px}.nofis .nofi-buttons>button:last-of-type{margin-right:0}"]
            }] }
];
/** @nocollapse */
NofiComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    NofiComponent.prototype.NOFI_TYPES;
    /** @type {?} */
    NofiComponent.prototype.faTimesCircle;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._notifications;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._onNotification;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ub2ZpLyIsInNvdXJjZXMiOlsibGliL25vZmkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBQyxJQUFJLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDaEUsT0FBTyxFQUFxQixVQUFVLEVBQUMsTUFBTSxRQUFRLENBQUM7QUFPdEQsTUFBTSxPQUFPLGFBQWE7SUFheEI7UUFYZ0IsZUFBVSxHQUFHLFVBQVUsQ0FBQzs7UUFHakMsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFNN0IsbUJBQWMsR0FBWSxFQUFFLENBQUM7UUFzQjdCLG9CQUFlOzs7O1FBQUcsQ0FBQyxJQUFXLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUVuQyxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDeEMsVUFBVSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFM0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsQ0FBQyxFQUFDO0lBM0JjLENBQUM7Ozs7SUFOakIsSUFBVyxhQUFhO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDOzs7O0lBTUQsUUFBUTtJQUNSLENBQUM7Ozs7O0lBRU0sWUFBWSxDQUFDLE1BQWM7UUFDaEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7OztJQUVNLGNBQWMsQ0FBQyxJQUFXLEVBQUUsT0FBb0I7UUFDckQsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxPQUFPLENBQUMsUUFBUSxLQUFLLFVBQVU7Z0JBQzVELE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDaEIsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLE9BQU8sSUFBSSxDQUFDLFFBQVEsS0FBSyxVQUFVO2dCQUMzRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFbEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7OztJQVdPLG9CQUFvQixDQUFDLE1BQWM7UUFDekMsd0ZBQXdGO1FBRXhGLElBQUksQ0FBQyxNQUFNO1lBQ1QsT0FBTzs7Y0FFSCxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxDQUFRLEVBQUUsRUFBRTtZQUN2RCxPQUFPLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDO1FBQ3pCLENBQUMsRUFBQztRQUVGLElBQUksS0FBSyxJQUFJLENBQUM7WUFDWixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7O1lBM0RGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsNGxDQUEwQjs7YUFFM0I7Ozs7OztJQUdDLG1DQUF3Qzs7SUFHeEMsc0NBQXFDOzs7OztJQU1yQyx1Q0FBcUM7Ozs7O0lBc0JyQyx3Q0FPRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0d1aWR9IGZyb20gJ2d1aWQtdHlwZXNjcmlwdCc7XG5pbXBvcnQge2ZhVGltZXNDaXJjbGV9IGZyb20gJ0Bmb3J0YXdlc29tZS9mcmVlLXNvbGlkLXN2Zy1pY29ucyc7XG5pbXBvcnQge0lOb2ZpLCBJTm9maUJ1dHRvbiwgTk9GSV9UWVBFU30gZnJvbSAnLi9ub2ZpJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLW5vZmknLFxuICB0ZW1wbGF0ZVVybDogJy4vbm9maS5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbm9maS5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTm9maUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgcHVibGljIHJlYWRvbmx5IE5PRklfVFlQRVMgPSBOT0ZJX1RZUEVTO1xuXG4gIC8vIEljb25zXG4gIHB1YmxpYyBmYVRpbWVzQ2lyY2xlID0gZmFUaW1lc0NpcmNsZTtcblxuICBwdWJsaWMgZ2V0IG5vdGlmaWNhdGlvbnMoKSB7XG4gICAgcmV0dXJuIHRoaXMuX25vdGlmaWNhdGlvbnM7XG4gIH1cblxuICBwcml2YXRlIF9ub3RpZmljYXRpb25zOiBJTm9maVtdID0gW107XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHB1YmxpYyBvbkNsb3NlQ2xpY2sobm9maUlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLl90aW1lb3V0Tm90aWZpY2F0aW9uKG5vZmlJZCk7XG4gIH1cblxuICBwdWJsaWMgb25Ob2ZpQnRuQ2xpY2sobm9maTogSU5vZmksIG5vZmlCdG46IElOb2ZpQnV0dG9uKSB7XG4gICAgaWYgKG5vZmkpIHtcbiAgICAgIGlmIChub2ZpQnRuLmNhbGxiYWNrICYmIHR5cGVvZiBub2ZpQnRuLmNhbGxiYWNrID09PSAnZnVuY3Rpb24nKVxuICAgICAgICBub2ZpQnRuLmNhbGxiYWNrKCk7XG4gICAgICBlbHNlIGlmIChub2ZpLmNhbGxiYWNrICYmIHR5cGVvZiBub2ZpLmNhbGxiYWNrID09PSAnZnVuY3Rpb24nKVxuICAgICAgICBub2ZpLmNhbGxiYWNrKCk7XG5cbiAgICAgIHRoaXMuX3RpbWVvdXROb3RpZmljYXRpb24obm9maS5pZCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfb25Ob3RpZmljYXRpb24gPSAobm9maTogSU5vZmkpID0+IHtcbiAgICBub2ZpLmlkID0gR3VpZC5jcmVhdGUoKS50b1N0cmluZygpO1xuXG4gICAgaWYgKG5vZmkuZHVyYXRpb24gJiYgIWlzTmFOKG5vZmkuZHVyYXRpb24pKVxuICAgICAgc2V0VGltZW91dCh0aGlzLl90aW1lb3V0Tm90aWZpY2F0aW9uLmJpbmQodGhpcywgbm9maS5pZCksIG5vZmkuZHVyYXRpb24pO1xuXG4gICAgdGhpcy5fbm90aWZpY2F0aW9ucy5wdXNoKG5vZmkpO1xuICB9O1xuXG4gIHByaXZhdGUgX3RpbWVvdXROb3RpZmljYXRpb24obm9maUlkOiBzdHJpbmcpIHtcbiAgICAvLyB0aGlzLl9sb2dnZXIuZGVidWcoYCR7dGhpcy5DTEFTU05BTUV9ID4gX3RpbWVvdXROb3RpZmljYXRpb24oKSA+IG5vZmlJZDogJHtub2ZpSWR9YCk7XG5cbiAgICBpZiAoIW5vZmlJZClcbiAgICAgIHJldHVybjtcblxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5fbm90aWZpY2F0aW9ucy5maW5kSW5kZXgoKG46IElOb2ZpKSA9PiB7XG4gICAgICByZXR1cm4gbi5pZCA9PT0gbm9maUlkO1xuICAgIH0pO1xuXG4gICAgaWYgKGluZGV4ID49IDApXG4gICAgICB0aGlzLl9ub3RpZmljYXRpb25zLnNwbGljZShpbmRleCwgMSk7XG4gIH1cbn1cbiJdfQ==