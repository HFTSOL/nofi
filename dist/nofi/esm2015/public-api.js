/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of nofi
 */
export { NofiService } from './lib/nofi.service';
export { NofiComponent } from './lib/nofi.component';
export { NofiModule } from './lib/nofi.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25vZmkvIiwic291cmNlcyI6WyJwdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSw0QkFBYyxvQkFBb0IsQ0FBQztBQUNuQyw4QkFBYyxzQkFBc0IsQ0FBQztBQUNyQywyQkFBYyxtQkFBbUIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2Ygbm9maVxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL25vZmkuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9ub2ZpLmNvbXBvbmVudCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9ub2ZpLm1vZHVsZSc7XG4iXX0=