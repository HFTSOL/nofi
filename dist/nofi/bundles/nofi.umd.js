(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('guid-typescript'), require('@fortawesome/free-solid-svg-icons'), require('@angular/common'), require('@fortawesome/angular-fontawesome')) :
    typeof define === 'function' && define.amd ? define('nofi', ['exports', '@angular/core', 'rxjs', 'guid-typescript', '@fortawesome/free-solid-svg-icons', '@angular/common', '@fortawesome/angular-fontawesome'], factory) :
    (global = global || self, factory(global.nofi = {}, global.ng.core, global.rxjs, global.guidTypescript, global.freeSolidSvgIcons, global.ng.common, global.angularFontawesome));
}(this, (function (exports, core, rxjs, guidTypescript, freeSolidSvgIcons, common, angularFontawesome) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // Nofi
    /**
     * @record
     */
    function INofi() { }
    if (false) {
        /** @type {?|undefined} */
        INofi.prototype.id;
        /** @type {?} */
        INofi.prototype.title;
        /** @type {?} */
        INofi.prototype.message;
        /** @type {?|undefined} */
        INofi.prototype.duration;
        /** @type {?|undefined} */
        INofi.prototype.isSticky;
        /** @type {?|undefined} */
        INofi.prototype.isNavSticky;
        /** @type {?|undefined} */
        INofi.prototype.isHtml;
        /** @type {?|undefined} */
        INofi.prototype.buttons;
        /** @type {?|undefined} */
        INofi.prototype.callback;
        /** @type {?} */
        INofi.prototype.type;
    }
    /**
     * @record
     */
    function INofiButton() { }
    if (false) {
        /** @type {?|undefined} */
        INofiButton.prototype.classes;
        /** @type {?|undefined} */
        INofiButton.prototype.altText;
        /** @type {?} */
        INofiButton.prototype.text;
        /** @type {?|undefined} */
        INofiButton.prototype.callback;
    }
    /** @enum {number} */
    var NOFI_TYPES = {
        Vanilla: 0,
        Success: 1,
        Info: 2,
        Warning: 3,
        Failure: 4,
    };
    NOFI_TYPES[NOFI_TYPES.Vanilla] = 'Vanilla';
    NOFI_TYPES[NOFI_TYPES.Success] = 'Success';
    NOFI_TYPES[NOFI_TYPES.Info] = 'Info';
    NOFI_TYPES[NOFI_TYPES.Warning] = 'Warning';
    NOFI_TYPES[NOFI_TYPES.Failure] = 'Failure';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NofiService = /** @class */ (function () {
        function NofiService() {
            this._notifier = null;
        }
        /**
         * @param {?} notification
         * @return {?}
         */
        NofiService.prototype.notify = /**
         * @param {?} notification
         * @return {?}
         */
        function (notification) {
            if (this._notifier) {
                this._notifier.next(notification);
            }
        };
        /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        NofiService.prototype.success = /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        function (title, message, duration) {
            if (duration === void 0) { duration = 25000; }
            this.notify({
                type: NOFI_TYPES.Success,
                title: title,
                message: message,
                duration: duration
            });
        };
        /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        NofiService.prototype.info = /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        function (title, message, duration) {
            if (duration === void 0) { duration = 25000; }
            this.notify({
                type: NOFI_TYPES.Info,
                title: title,
                message: message,
                duration: duration
            });
        };
        /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        NofiService.prototype.warning = /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        function (title, message, duration) {
            if (duration === void 0) { duration = 25000; }
            this.notify({
                type: NOFI_TYPES.Warning,
                title: title,
                message: message,
                duration: duration
            });
        };
        /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        NofiService.prototype.failure = /**
         * @param {?} title
         * @param {?} message
         * @param {?=} duration
         * @return {?}
         */
        function (title, message, duration) {
            if (duration === void 0) { duration = 25000; }
            this.notify({
                type: NOFI_TYPES.Failure,
                title: title,
                message: message,
                duration: duration
            });
        };
        /**
         * @return {?}
         */
        NofiService.prototype.register = /**
         * @return {?}
         */
        function () {
            this._notifier = new rxjs.Subject();
            return this._notifier;
        };
        NofiService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        NofiService.ctorParameters = function () { return []; };
        /** @nocollapse */ NofiService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NofiService_Factory() { return new NofiService(); }, token: NofiService, providedIn: "root" });
        return NofiService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        NofiService.prototype._notifier;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NofiComponent = /** @class */ (function () {
        function NofiComponent() {
            var _this = this;
            this.NOFI_TYPES = NOFI_TYPES;
            // Icons
            this.faTimesCircle = freeSolidSvgIcons.faTimesCircle;
            this._notifications = [];
            this._onNotification = (/**
             * @param {?} nofi
             * @return {?}
             */
            function (nofi) {
                nofi.id = guidTypescript.Guid.create().toString();
                if (nofi.duration && !isNaN(nofi.duration))
                    setTimeout(_this._timeoutNotification.bind(_this, nofi.id), nofi.duration);
                _this._notifications.push(nofi);
            });
        }
        Object.defineProperty(NofiComponent.prototype, "notifications", {
            get: /**
             * @return {?}
             */
            function () {
                return this._notifications;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        NofiComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} nofiId
         * @return {?}
         */
        NofiComponent.prototype.onCloseClick = /**
         * @param {?} nofiId
         * @return {?}
         */
        function (nofiId) {
            this._timeoutNotification(nofiId);
        };
        /**
         * @param {?} nofi
         * @param {?} nofiBtn
         * @return {?}
         */
        NofiComponent.prototype.onNofiBtnClick = /**
         * @param {?} nofi
         * @param {?} nofiBtn
         * @return {?}
         */
        function (nofi, nofiBtn) {
            if (nofi) {
                if (nofiBtn.callback && typeof nofiBtn.callback === 'function')
                    nofiBtn.callback();
                else if (nofi.callback && typeof nofi.callback === 'function')
                    nofi.callback();
                this._timeoutNotification(nofi.id);
            }
        };
        /**
         * @private
         * @param {?} nofiId
         * @return {?}
         */
        NofiComponent.prototype._timeoutNotification = /**
         * @private
         * @param {?} nofiId
         * @return {?}
         */
        function (nofiId) {
            // this._logger.debug(`${this.CLASSNAME} > _timeoutNotification() > nofiId: ${nofiId}`);
            if (!nofiId)
                return;
            /** @type {?} */
            var index = this._notifications.findIndex((/**
             * @param {?} n
             * @return {?}
             */
            function (n) {
                return n.id === nofiId;
            }));
            if (index >= 0)
                this._notifications.splice(index, 1);
        };
        NofiComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-nofi',
                        template: "\r\n<div class=\"nofis\">\r\n  <ul>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"!nofi.buttons || !nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <fa-icon class=\"nofi-x\" [icon]=\"faTimesCircle\" (click)=\"onCloseClick(nofi.id)\"></fa-icon>\r\n      </div>\r\n    </li>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"nofi.buttons && nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <div class=\"nofi-buttons\">\r\n          <button *ngFor=\"let nofiBtn of nofi.buttons\"\r\n                  [class]=\"nofiBtn.classes || ''\"\r\n                  [title]=\"nofiBtn.altText || ''\"\r\n                  (click)=\"onNofiBtnClick(nofi, nofiBtn)\">\r\n            {{nofiBtn.text}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>\r\n",
                        styles: [".nofis{position:fixed;right:10px;top:50px;width:250px;z-index:1000}.nofis>ul>li{margin-bottom:10px}.nofis .nofi{background-color:#030303;border:1px solid #fff;padding:10px;position:relative;border-radius:12px;box-shadow:10px 10px 20px 0 rgba(0,0,0,.75)}.nofis .nofi.success{background-color:#51a351}.nofis .nofi.info{background-color:#2f96b4}.nofis .nofi.warning{background-color:#f89406}.nofis .nofi.failure{background-color:#bd362f}.nofis .nofi-title{color:#fff;font-weight:600;font-size:14px}.nofis .nofi-msg{color:#fff;font-size:12px;margin-bottom:5px}.nofis .nofi-x{color:#fff;font-size:16px;position:absolute;right:10px;top:10px;z-index:1010}.nofis .nofi-buttons{text-align:center;width:100%}.nofis .nofi-buttons>button{margin-bottom:3px;margin-right:10px}.nofis .nofi-buttons>button:last-of-type{margin-right:0}"]
                    }] }
        ];
        /** @nocollapse */
        NofiComponent.ctorParameters = function () { return []; };
        return NofiComponent;
    }());
    if (false) {
        /** @type {?} */
        NofiComponent.prototype.NOFI_TYPES;
        /** @type {?} */
        NofiComponent.prototype.faTimesCircle;
        /**
         * @type {?}
         * @private
         */
        NofiComponent.prototype._notifications;
        /**
         * @type {?}
         * @private
         */
        NofiComponent.prototype._onNotification;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NotifierTypePipe = /** @class */ (function () {
        function NotifierTypePipe() {
        }
        /**
         * @param {?} nofi
         * @return {?}
         */
        NotifierTypePipe.prototype.transform = /**
         * @param {?} nofi
         * @return {?}
         */
        function (nofi) {
            switch (nofi.type) {
                case NOFI_TYPES.Vanilla:
                    return '';
                case NOFI_TYPES.Success:
                    return 'success';
                case NOFI_TYPES.Info:
                    return 'info';
                case NOFI_TYPES.Warning:
                    return 'warning';
                case NOFI_TYPES.Failure:
                    return 'failure';
            }
        };
        NotifierTypePipe.decorators = [
            { type: core.Pipe, args: [{ name: 'notifierType' },] }
        ];
        return NotifierTypePipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NofiModule = /** @class */ (function () {
        function NofiModule() {
        }
        NofiModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            NofiComponent,
                            NotifierTypePipe
                        ],
                        imports: [
                            common.CommonModule,
                            angularFontawesome.FontAwesomeModule,
                        ],
                        exports: [NofiComponent]
                    },] }
        ];
        return NofiModule;
    }());

    exports.NofiComponent = NofiComponent;
    exports.NofiModule = NofiModule;
    exports.NofiService = NofiService;
    exports.ɵa = NotifierTypePipe;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=nofi.umd.js.map
