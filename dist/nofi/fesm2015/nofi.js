import { Injectable, ɵɵdefineInjectable, Component, Pipe, NgModule } from '@angular/core';
import { Subject } from 'rxjs';
import { Guid } from 'guid-typescript';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Nofi
/**
 * @record
 */
function INofi() { }
if (false) {
    /** @type {?|undefined} */
    INofi.prototype.id;
    /** @type {?} */
    INofi.prototype.title;
    /** @type {?} */
    INofi.prototype.message;
    /** @type {?|undefined} */
    INofi.prototype.duration;
    /** @type {?|undefined} */
    INofi.prototype.isSticky;
    /** @type {?|undefined} */
    INofi.prototype.isNavSticky;
    /** @type {?|undefined} */
    INofi.prototype.isHtml;
    /** @type {?|undefined} */
    INofi.prototype.buttons;
    /** @type {?|undefined} */
    INofi.prototype.callback;
    /** @type {?} */
    INofi.prototype.type;
}
/**
 * @record
 */
function INofiButton() { }
if (false) {
    /** @type {?|undefined} */
    INofiButton.prototype.classes;
    /** @type {?|undefined} */
    INofiButton.prototype.altText;
    /** @type {?} */
    INofiButton.prototype.text;
    /** @type {?|undefined} */
    INofiButton.prototype.callback;
}
/** @enum {number} */
const NOFI_TYPES = {
    Vanilla: 0,
    Success: 1,
    Info: 2,
    Warning: 3,
    Failure: 4,
};
NOFI_TYPES[NOFI_TYPES.Vanilla] = 'Vanilla';
NOFI_TYPES[NOFI_TYPES.Success] = 'Success';
NOFI_TYPES[NOFI_TYPES.Info] = 'Info';
NOFI_TYPES[NOFI_TYPES.Warning] = 'Warning';
NOFI_TYPES[NOFI_TYPES.Failure] = 'Failure';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NofiService {
    constructor() {
        this._notifier = null;
    }
    /**
     * @param {?} notification
     * @return {?}
     */
    notify(notification) {
        if (this._notifier) {
            this._notifier.next(notification);
        }
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    success(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Success,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    info(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Info,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    warning(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Warning,
            title,
            message,
            duration
        });
    }
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    failure(title, message, duration = 25000) {
        this.notify({
            type: NOFI_TYPES.Failure,
            title,
            message,
            duration
        });
    }
    /**
     * @return {?}
     */
    register() {
        this._notifier = new Subject();
        return this._notifier;
    }
}
NofiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NofiService.ctorParameters = () => [];
/** @nocollapse */ NofiService.ngInjectableDef = ɵɵdefineInjectable({ factory: function NofiService_Factory() { return new NofiService(); }, token: NofiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NofiService.prototype._notifier;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NofiComponent {
    constructor() {
        this.NOFI_TYPES = NOFI_TYPES;
        // Icons
        this.faTimesCircle = faTimesCircle;
        this._notifications = [];
        this._onNotification = (/**
         * @param {?} nofi
         * @return {?}
         */
        (nofi) => {
            nofi.id = Guid.create().toString();
            if (nofi.duration && !isNaN(nofi.duration))
                setTimeout(this._timeoutNotification.bind(this, nofi.id), nofi.duration);
            this._notifications.push(nofi);
        });
    }
    /**
     * @return {?}
     */
    get notifications() {
        return this._notifications;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} nofiId
     * @return {?}
     */
    onCloseClick(nofiId) {
        this._timeoutNotification(nofiId);
    }
    /**
     * @param {?} nofi
     * @param {?} nofiBtn
     * @return {?}
     */
    onNofiBtnClick(nofi, nofiBtn) {
        if (nofi) {
            if (nofiBtn.callback && typeof nofiBtn.callback === 'function')
                nofiBtn.callback();
            else if (nofi.callback && typeof nofi.callback === 'function')
                nofi.callback();
            this._timeoutNotification(nofi.id);
        }
    }
    /**
     * @private
     * @param {?} nofiId
     * @return {?}
     */
    _timeoutNotification(nofiId) {
        // this._logger.debug(`${this.CLASSNAME} > _timeoutNotification() > nofiId: ${nofiId}`);
        if (!nofiId)
            return;
        /** @type {?} */
        const index = this._notifications.findIndex((/**
         * @param {?} n
         * @return {?}
         */
        (n) => {
            return n.id === nofiId;
        }));
        if (index >= 0)
            this._notifications.splice(index, 1);
    }
}
NofiComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-nofi',
                template: "\r\n<div class=\"nofis\">\r\n  <ul>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"!nofi.buttons || !nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <fa-icon class=\"nofi-x\" [icon]=\"faTimesCircle\" (click)=\"onCloseClick(nofi.id)\"></fa-icon>\r\n      </div>\r\n    </li>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"nofi.buttons && nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <div class=\"nofi-buttons\">\r\n          <button *ngFor=\"let nofiBtn of nofi.buttons\"\r\n                  [class]=\"nofiBtn.classes || ''\"\r\n                  [title]=\"nofiBtn.altText || ''\"\r\n                  (click)=\"onNofiBtnClick(nofi, nofiBtn)\">\r\n            {{nofiBtn.text}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>\r\n",
                styles: [".nofis{position:fixed;right:10px;top:50px;width:250px;z-index:1000}.nofis>ul>li{margin-bottom:10px}.nofis .nofi{background-color:#030303;border:1px solid #fff;padding:10px;position:relative;border-radius:12px;box-shadow:10px 10px 20px 0 rgba(0,0,0,.75)}.nofis .nofi.success{background-color:#51a351}.nofis .nofi.info{background-color:#2f96b4}.nofis .nofi.warning{background-color:#f89406}.nofis .nofi.failure{background-color:#bd362f}.nofis .nofi-title{color:#fff;font-weight:600;font-size:14px}.nofis .nofi-msg{color:#fff;font-size:12px;margin-bottom:5px}.nofis .nofi-x{color:#fff;font-size:16px;position:absolute;right:10px;top:10px;z-index:1010}.nofis .nofi-buttons{text-align:center;width:100%}.nofis .nofi-buttons>button{margin-bottom:3px;margin-right:10px}.nofis .nofi-buttons>button:last-of-type{margin-right:0}"]
            }] }
];
/** @nocollapse */
NofiComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    NofiComponent.prototype.NOFI_TYPES;
    /** @type {?} */
    NofiComponent.prototype.faTimesCircle;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._notifications;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._onNotification;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NotifierTypePipe {
    /**
     * @param {?} nofi
     * @return {?}
     */
    transform(nofi) {
        switch (nofi.type) {
            case NOFI_TYPES.Vanilla:
                return '';
            case NOFI_TYPES.Success:
                return 'success';
            case NOFI_TYPES.Info:
                return 'info';
            case NOFI_TYPES.Warning:
                return 'warning';
            case NOFI_TYPES.Failure:
                return 'failure';
        }
    }
}
NotifierTypePipe.decorators = [
    { type: Pipe, args: [{ name: 'notifierType' },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NofiModule {
}
NofiModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    NofiComponent,
                    NotifierTypePipe
                ],
                imports: [
                    CommonModule,
                    FontAwesomeModule,
                ],
                exports: [NofiComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { NofiComponent, NofiModule, NofiService, NotifierTypePipe as ɵa };
//# sourceMappingURL=nofi.js.map
