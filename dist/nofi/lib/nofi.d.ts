export interface INofi {
    id?: string;
    title: string;
    message: string;
    duration?: number;
    isSticky?: boolean;
    isNavSticky?: boolean;
    isHtml?: boolean;
    buttons?: any[];
    callback?: any;
    type: NOFI_TYPES;
}
export interface INofiButton {
    classes?: string;
    altText?: string;
    text: string;
    callback?: any;
}
export declare enum NOFI_TYPES {
    Vanilla = 0,
    Success = 1,
    Info = 2,
    Warning = 3,
    Failure = 4
}
