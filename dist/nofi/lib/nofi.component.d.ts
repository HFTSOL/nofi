import { OnInit } from '@angular/core';
import { INofi, INofiButton, NOFI_TYPES } from './nofi';
export declare class NofiComponent implements OnInit {
    readonly NOFI_TYPES: typeof NOFI_TYPES;
    faTimesCircle: import("@fortawesome/fontawesome-common-types").IconDefinition;
    readonly notifications: INofi[];
    private _notifications;
    constructor();
    ngOnInit(): void;
    onCloseClick(nofiId: string): void;
    onNofiBtnClick(nofi: INofi, nofiBtn: INofiButton): void;
    private _onNotification;
    private _timeoutNotification;
}
