import { Observable } from 'rxjs';
import { INofi } from './nofi';
export declare class NofiService {
    private _notifier;
    constructor();
    notify(notification: INofi): void;
    success(title: string, message: string, duration?: number): void;
    info(title: string, message: string, duration?: number): void;
    warning(title: string, message: string, duration?: number): void;
    failure(title: string, message: string, duration?: number): void;
    register(): Observable<INofi>;
}
