import { PipeTransform } from '@angular/core';
import { INofi } from '../nofi';
export declare class NotifierTypePipe implements PipeTransform {
    transform(nofi: INofi): string;
}
