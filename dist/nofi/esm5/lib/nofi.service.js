/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { NOFI_TYPES } from './nofi';
import * as i0 from "@angular/core";
var NofiService = /** @class */ (function () {
    function NofiService() {
        this._notifier = null;
    }
    /**
     * @param {?} notification
     * @return {?}
     */
    NofiService.prototype.notify = /**
     * @param {?} notification
     * @return {?}
     */
    function (notification) {
        if (this._notifier) {
            this._notifier.next(notification);
        }
    };
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    NofiService.prototype.success = /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    function (title, message, duration) {
        if (duration === void 0) { duration = 25000; }
        this.notify({
            type: NOFI_TYPES.Success,
            title: title,
            message: message,
            duration: duration
        });
    };
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    NofiService.prototype.info = /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    function (title, message, duration) {
        if (duration === void 0) { duration = 25000; }
        this.notify({
            type: NOFI_TYPES.Info,
            title: title,
            message: message,
            duration: duration
        });
    };
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    NofiService.prototype.warning = /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    function (title, message, duration) {
        if (duration === void 0) { duration = 25000; }
        this.notify({
            type: NOFI_TYPES.Warning,
            title: title,
            message: message,
            duration: duration
        });
    };
    /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    NofiService.prototype.failure = /**
     * @param {?} title
     * @param {?} message
     * @param {?=} duration
     * @return {?}
     */
    function (title, message, duration) {
        if (duration === void 0) { duration = 25000; }
        this.notify({
            type: NOFI_TYPES.Failure,
            title: title,
            message: message,
            duration: duration
        });
    };
    /**
     * @return {?}
     */
    NofiService.prototype.register = /**
     * @return {?}
     */
    function () {
        this._notifier = new Subject();
        return this._notifier;
    };
    NofiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NofiService.ctorParameters = function () { return []; };
    /** @nocollapse */ NofiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NofiService_Factory() { return new NofiService(); }, token: NofiService, providedIn: "root" });
    return NofiService;
}());
export { NofiService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NofiService.prototype._notifier;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbm9maS8iLCJzb3VyY2VzIjpbImxpYi9ub2ZpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFhLE9BQU8sRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUN6QyxPQUFPLEVBQVEsVUFBVSxFQUFDLE1BQU0sUUFBUSxDQUFDOztBQUV6QztJQU1FO1FBRlEsY0FBUyxHQUFtQixJQUFJLENBQUM7SUFHekMsQ0FBQzs7Ozs7SUFFTSw0QkFBTTs7OztJQUFiLFVBQWMsWUFBbUI7UUFDL0IsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ25DO0lBQ0gsQ0FBQzs7Ozs7OztJQUVNLDZCQUFPOzs7Ozs7SUFBZCxVQUFlLEtBQWEsRUFBRSxPQUFlLEVBQUUsUUFBZ0I7UUFBaEIseUJBQUEsRUFBQSxnQkFBZ0I7UUFDN0QsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNWLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTztZQUN4QixLQUFLLE9BQUE7WUFDTCxPQUFPLFNBQUE7WUFDUCxRQUFRLFVBQUE7U0FDVCxDQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7O0lBRU0sMEJBQUk7Ozs7OztJQUFYLFVBQVksS0FBYSxFQUFFLE9BQWUsRUFBRSxRQUFnQjtRQUFoQix5QkFBQSxFQUFBLGdCQUFnQjtRQUMxRCxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ1YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFJO1lBQ3JCLEtBQUssT0FBQTtZQUNMLE9BQU8sU0FBQTtZQUNQLFFBQVEsVUFBQTtTQUNULENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFTSw2QkFBTzs7Ozs7O0lBQWQsVUFBZSxLQUFhLEVBQUUsT0FBZSxFQUFFLFFBQWdCO1FBQWhCLHlCQUFBLEVBQUEsZ0JBQWdCO1FBQzdELElBQUksQ0FBQyxNQUFNLENBQUM7WUFDVixJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU87WUFDeEIsS0FBSyxPQUFBO1lBQ0wsT0FBTyxTQUFBO1lBQ1AsUUFBUSxVQUFBO1NBQ1QsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVNLDZCQUFPOzs7Ozs7SUFBZCxVQUFlLEtBQWEsRUFBRSxPQUFlLEVBQUUsUUFBZ0I7UUFBaEIseUJBQUEsRUFBQSxnQkFBZ0I7UUFDN0QsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNWLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTztZQUN4QixLQUFLLE9BQUE7WUFDTCxPQUFPLFNBQUE7WUFDUCxRQUFRLFVBQUE7U0FDVCxDQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRU0sOEJBQVE7OztJQUFmO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDOztnQkF0REYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7c0JBTkQ7Q0EyREMsQUF2REQsSUF1REM7U0FwRFksV0FBVzs7Ozs7O0lBQ3RCLGdDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge09ic2VydmFibGUsIFN1YmplY3R9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtJTm9maSwgTk9GSV9UWVBFU30gZnJvbSAnLi9ub2ZpJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTm9maVNlcnZpY2Uge1xuICBwcml2YXRlIF9ub3RpZmllcjogU3ViamVjdDxJTm9maT4gPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgcHVibGljIG5vdGlmeShub3RpZmljYXRpb246IElOb2ZpKSB7XG4gICAgaWYgKHRoaXMuX25vdGlmaWVyKSB7XG4gICAgICB0aGlzLl9ub3RpZmllci5uZXh0KG5vdGlmaWNhdGlvbik7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHN1Y2Nlc3ModGl0bGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nLCBkdXJhdGlvbiA9IDI1MDAwKSB7XG4gICAgdGhpcy5ub3RpZnkoe1xuICAgICAgdHlwZTogTk9GSV9UWVBFUy5TdWNjZXNzLFxuICAgICAgdGl0bGUsXG4gICAgICBtZXNzYWdlLFxuICAgICAgZHVyYXRpb25cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBpbmZvKHRpdGxlOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZywgZHVyYXRpb24gPSAyNTAwMCkge1xuICAgIHRoaXMubm90aWZ5KHtcbiAgICAgIHR5cGU6IE5PRklfVFlQRVMuSW5mbyxcbiAgICAgIHRpdGxlLFxuICAgICAgbWVzc2FnZSxcbiAgICAgIGR1cmF0aW9uXG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgd2FybmluZyh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIGR1cmF0aW9uID0gMjUwMDApIHtcbiAgICB0aGlzLm5vdGlmeSh7XG4gICAgICB0eXBlOiBOT0ZJX1RZUEVTLldhcm5pbmcsXG4gICAgICB0aXRsZSxcbiAgICAgIG1lc3NhZ2UsXG4gICAgICBkdXJhdGlvblxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGZhaWx1cmUodGl0bGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nLCBkdXJhdGlvbiA9IDI1MDAwKSB7XG4gICAgdGhpcy5ub3RpZnkoe1xuICAgICAgdHlwZTogTk9GSV9UWVBFUy5GYWlsdXJlLFxuICAgICAgdGl0bGUsXG4gICAgICBtZXNzYWdlLFxuICAgICAgZHVyYXRpb25cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyByZWdpc3RlcigpOiBPYnNlcnZhYmxlPElOb2ZpPiB7XG4gICAgdGhpcy5fbm90aWZpZXIgPSBuZXcgU3ViamVjdDxJTm9maT4oKTtcbiAgICByZXR1cm4gdGhpcy5fbm90aWZpZXI7XG4gIH1cbn1cbiJdfQ==