/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Guid } from 'guid-typescript';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { NOFI_TYPES } from './nofi';
var NofiComponent = /** @class */ (function () {
    function NofiComponent() {
        var _this = this;
        this.NOFI_TYPES = NOFI_TYPES;
        // Icons
        this.faTimesCircle = faTimesCircle;
        this._notifications = [];
        this._onNotification = (/**
         * @param {?} nofi
         * @return {?}
         */
        function (nofi) {
            nofi.id = Guid.create().toString();
            if (nofi.duration && !isNaN(nofi.duration))
                setTimeout(_this._timeoutNotification.bind(_this, nofi.id), nofi.duration);
            _this._notifications.push(nofi);
        });
    }
    Object.defineProperty(NofiComponent.prototype, "notifications", {
        get: /**
         * @return {?}
         */
        function () {
            return this._notifications;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NofiComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} nofiId
     * @return {?}
     */
    NofiComponent.prototype.onCloseClick = /**
     * @param {?} nofiId
     * @return {?}
     */
    function (nofiId) {
        this._timeoutNotification(nofiId);
    };
    /**
     * @param {?} nofi
     * @param {?} nofiBtn
     * @return {?}
     */
    NofiComponent.prototype.onNofiBtnClick = /**
     * @param {?} nofi
     * @param {?} nofiBtn
     * @return {?}
     */
    function (nofi, nofiBtn) {
        if (nofi) {
            if (nofiBtn.callback && typeof nofiBtn.callback === 'function')
                nofiBtn.callback();
            else if (nofi.callback && typeof nofi.callback === 'function')
                nofi.callback();
            this._timeoutNotification(nofi.id);
        }
    };
    /**
     * @private
     * @param {?} nofiId
     * @return {?}
     */
    NofiComponent.prototype._timeoutNotification = /**
     * @private
     * @param {?} nofiId
     * @return {?}
     */
    function (nofiId) {
        // this._logger.debug(`${this.CLASSNAME} > _timeoutNotification() > nofiId: ${nofiId}`);
        if (!nofiId)
            return;
        /** @type {?} */
        var index = this._notifications.findIndex((/**
         * @param {?} n
         * @return {?}
         */
        function (n) {
            return n.id === nofiId;
        }));
        if (index >= 0)
            this._notifications.splice(index, 1);
    };
    NofiComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-nofi',
                    template: "\r\n<div class=\"nofis\">\r\n  <ul>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"!nofi.buttons || !nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <fa-icon class=\"nofi-x\" [icon]=\"faTimesCircle\" (click)=\"onCloseClick(nofi.id)\"></fa-icon>\r\n      </div>\r\n    </li>\r\n    <li *ngFor=\"let nofi of notifications\">\r\n      <div class=\"nofi {{nofi | notifierType}}\" *ngIf=\"nofi.buttons && nofi.buttons.length\">\r\n        <label class=\"nofi-title\">{{nofi.title}}</label>\r\n        <p class=\"nofi-msg\">{{nofi.message}}</p>\r\n        <div class=\"nofi-buttons\">\r\n          <button *ngFor=\"let nofiBtn of nofi.buttons\"\r\n                  [class]=\"nofiBtn.classes || ''\"\r\n                  [title]=\"nofiBtn.altText || ''\"\r\n                  (click)=\"onNofiBtnClick(nofi, nofiBtn)\">\r\n            {{nofiBtn.text}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>\r\n",
                    styles: [".nofis{position:fixed;right:10px;top:50px;width:250px;z-index:1000}.nofis>ul>li{margin-bottom:10px}.nofis .nofi{background-color:#030303;border:1px solid #fff;padding:10px;position:relative;border-radius:12px;box-shadow:10px 10px 20px 0 rgba(0,0,0,.75)}.nofis .nofi.success{background-color:#51a351}.nofis .nofi.info{background-color:#2f96b4}.nofis .nofi.warning{background-color:#f89406}.nofis .nofi.failure{background-color:#bd362f}.nofis .nofi-title{color:#fff;font-weight:600;font-size:14px}.nofis .nofi-msg{color:#fff;font-size:12px;margin-bottom:5px}.nofis .nofi-x{color:#fff;font-size:16px;position:absolute;right:10px;top:10px;z-index:1010}.nofis .nofi-buttons{text-align:center;width:100%}.nofis .nofi-buttons>button{margin-bottom:3px;margin-right:10px}.nofis .nofi-buttons>button:last-of-type{margin-right:0}"]
                }] }
    ];
    /** @nocollapse */
    NofiComponent.ctorParameters = function () { return []; };
    return NofiComponent;
}());
export { NofiComponent };
if (false) {
    /** @type {?} */
    NofiComponent.prototype.NOFI_TYPES;
    /** @type {?} */
    NofiComponent.prototype.faTimesCircle;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._notifications;
    /**
     * @type {?}
     * @private
     */
    NofiComponent.prototype._onNotification;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ub2ZpLyIsInNvdXJjZXMiOlsibGliL25vZmkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBQyxJQUFJLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDaEUsT0FBTyxFQUFxQixVQUFVLEVBQUMsTUFBTSxRQUFRLENBQUM7QUFFdEQ7SUFrQkU7UUFBQSxpQkFBaUI7UUFYRCxlQUFVLEdBQUcsVUFBVSxDQUFDOztRQUdqQyxrQkFBYSxHQUFHLGFBQWEsQ0FBQztRQU03QixtQkFBYyxHQUFZLEVBQUUsQ0FBQztRQXNCN0Isb0JBQWU7Ozs7UUFBRyxVQUFDLElBQVc7WUFDcEMsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFbkMsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3hDLFVBQVUsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEtBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTNFLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLENBQUMsRUFBQztJQTNCYyxDQUFDO0lBTmpCLHNCQUFXLHdDQUFhOzs7O1FBQXhCO1lBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzdCLENBQUM7OztPQUFBOzs7O0lBTUQsZ0NBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Ozs7SUFFTSxvQ0FBWTs7OztJQUFuQixVQUFvQixNQUFjO1FBQ2hDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFFTSxzQ0FBYzs7Ozs7SUFBckIsVUFBc0IsSUFBVyxFQUFFLE9BQW9CO1FBQ3JELElBQUksSUFBSSxFQUFFO1lBQ1IsSUFBSSxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU8sT0FBTyxDQUFDLFFBQVEsS0FBSyxVQUFVO2dCQUM1RCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ2hCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLElBQUksQ0FBQyxRQUFRLEtBQUssVUFBVTtnQkFDM0QsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBRWxCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7Ozs7SUFXTyw0Q0FBb0I7Ozs7O0lBQTVCLFVBQTZCLE1BQWM7UUFDekMsd0ZBQXdGO1FBRXhGLElBQUksQ0FBQyxNQUFNO1lBQ1QsT0FBTzs7WUFFSCxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxDQUFRO1lBQ25ELE9BQU8sQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUM7UUFDekIsQ0FBQyxFQUFDO1FBRUYsSUFBSSxLQUFLLElBQUksQ0FBQztZQUNaLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6QyxDQUFDOztnQkEzREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQiw0bENBQTBCOztpQkFFM0I7Ozs7SUF3REQsb0JBQUM7Q0FBQSxBQTVERCxJQTREQztTQXZEWSxhQUFhOzs7SUFFeEIsbUNBQXdDOztJQUd4QyxzQ0FBcUM7Ozs7O0lBTXJDLHVDQUFxQzs7Ozs7SUFzQnJDLHdDQU9FIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7R3VpZH0gZnJvbSAnZ3VpZC10eXBlc2NyaXB0JztcbmltcG9ydCB7ZmFUaW1lc0NpcmNsZX0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtc29saWQtc3ZnLWljb25zJztcbmltcG9ydCB7SU5vZmksIElOb2ZpQnV0dG9uLCBOT0ZJX1RZUEVTfSBmcm9tICcuL25vZmknO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItbm9maScsXG4gIHRlbXBsYXRlVXJsOiAnLi9ub2ZpLmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ub2ZpLnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBOb2ZpQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwdWJsaWMgcmVhZG9ubHkgTk9GSV9UWVBFUyA9IE5PRklfVFlQRVM7XG5cbiAgLy8gSWNvbnNcbiAgcHVibGljIGZhVGltZXNDaXJjbGUgPSBmYVRpbWVzQ2lyY2xlO1xuXG4gIHB1YmxpYyBnZXQgbm90aWZpY2F0aW9ucygpIHtcbiAgICByZXR1cm4gdGhpcy5fbm90aWZpY2F0aW9ucztcbiAgfVxuXG4gIHByaXZhdGUgX25vdGlmaWNhdGlvbnM6IElOb2ZpW10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgcHVibGljIG9uQ2xvc2VDbGljayhub2ZpSWQ6IHN0cmluZykge1xuICAgIHRoaXMuX3RpbWVvdXROb3RpZmljYXRpb24obm9maUlkKTtcbiAgfVxuXG4gIHB1YmxpYyBvbk5vZmlCdG5DbGljayhub2ZpOiBJTm9maSwgbm9maUJ0bjogSU5vZmlCdXR0b24pIHtcbiAgICBpZiAobm9maSkge1xuICAgICAgaWYgKG5vZmlCdG4uY2FsbGJhY2sgJiYgdHlwZW9mIG5vZmlCdG4uY2FsbGJhY2sgPT09ICdmdW5jdGlvbicpXG4gICAgICAgIG5vZmlCdG4uY2FsbGJhY2soKTtcbiAgICAgIGVsc2UgaWYgKG5vZmkuY2FsbGJhY2sgJiYgdHlwZW9mIG5vZmkuY2FsbGJhY2sgPT09ICdmdW5jdGlvbicpXG4gICAgICAgIG5vZmkuY2FsbGJhY2soKTtcblxuICAgICAgdGhpcy5fdGltZW91dE5vdGlmaWNhdGlvbihub2ZpLmlkKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIF9vbk5vdGlmaWNhdGlvbiA9IChub2ZpOiBJTm9maSkgPT4ge1xuICAgIG5vZmkuaWQgPSBHdWlkLmNyZWF0ZSgpLnRvU3RyaW5nKCk7XG5cbiAgICBpZiAobm9maS5kdXJhdGlvbiAmJiAhaXNOYU4obm9maS5kdXJhdGlvbikpXG4gICAgICBzZXRUaW1lb3V0KHRoaXMuX3RpbWVvdXROb3RpZmljYXRpb24uYmluZCh0aGlzLCBub2ZpLmlkKSwgbm9maS5kdXJhdGlvbik7XG5cbiAgICB0aGlzLl9ub3RpZmljYXRpb25zLnB1c2gobm9maSk7XG4gIH07XG5cbiAgcHJpdmF0ZSBfdGltZW91dE5vdGlmaWNhdGlvbihub2ZpSWQ6IHN0cmluZykge1xuICAgIC8vIHRoaXMuX2xvZ2dlci5kZWJ1ZyhgJHt0aGlzLkNMQVNTTkFNRX0gPiBfdGltZW91dE5vdGlmaWNhdGlvbigpID4gbm9maUlkOiAke25vZmlJZH1gKTtcblxuICAgIGlmICghbm9maUlkKVxuICAgICAgcmV0dXJuO1xuXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLl9ub3RpZmljYXRpb25zLmZpbmRJbmRleCgobjogSU5vZmkpID0+IHtcbiAgICAgIHJldHVybiBuLmlkID09PSBub2ZpSWQ7XG4gICAgfSk7XG5cbiAgICBpZiAoaW5kZXggPj0gMClcbiAgICAgIHRoaXMuX25vdGlmaWNhdGlvbnMuc3BsaWNlKGluZGV4LCAxKTtcbiAgfVxufVxuIl19