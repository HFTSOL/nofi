/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { NOFI_TYPES } from '../nofi';
var NotifierTypePipe = /** @class */ (function () {
    function NotifierTypePipe() {
    }
    /**
     * @param {?} nofi
     * @return {?}
     */
    NotifierTypePipe.prototype.transform = /**
     * @param {?} nofi
     * @return {?}
     */
    function (nofi) {
        switch (nofi.type) {
            case NOFI_TYPES.Vanilla:
                return '';
            case NOFI_TYPES.Success:
                return 'success';
            case NOFI_TYPES.Info:
                return 'info';
            case NOFI_TYPES.Warning:
                return 'warning';
            case NOFI_TYPES.Failure:
                return 'failure';
        }
    };
    NotifierTypePipe.decorators = [
        { type: Pipe, args: [{ name: 'notifierType' },] }
    ];
    return NotifierTypePipe;
}());
export { NotifierTypePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZS1jbGFzcy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbm9maS8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy90eXBlLWNsYXNzLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxJQUFJLEVBQWdCLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBUSxVQUFVLEVBQUMsTUFBTSxTQUFTLENBQUM7QUFFMUM7SUFBQTtJQW9CQSxDQUFDOzs7OztJQWxCQyxvQ0FBUzs7OztJQUFULFVBQVUsSUFBVztRQUNuQixRQUFPLElBQUksQ0FBQyxJQUFJLEVBQUM7WUFDZixLQUFLLFVBQVUsQ0FBQyxPQUFPO2dCQUNyQixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssVUFBVSxDQUFDLE9BQU87Z0JBQ3JCLE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssVUFBVSxDQUFDLElBQUk7Z0JBQ2xCLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssVUFBVSxDQUFDLE9BQU87Z0JBQ3JCLE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssVUFBVSxDQUFDLE9BQU87Z0JBQ3JCLE9BQU8sU0FBUyxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQzs7Z0JBbkJGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxjQUFjLEVBQUM7O0lBb0I1Qix1QkFBQztDQUFBLEFBcEJELElBb0JDO1NBbkJZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7UGlwZSwgUGlwZVRyYW5zZm9ybX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SU5vZmksIE5PRklfVFlQRVN9IGZyb20gJy4uL25vZmknO1xyXG5cclxuQFBpcGUoe25hbWU6ICdub3RpZmllclR5cGUnfSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWVyVHlwZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICB0cmFuc2Zvcm0obm9maTogSU5vZmkpOiBzdHJpbmcge1xyXG4gICAgc3dpdGNoKG5vZmkudHlwZSl7XHJcbiAgICAgIGNhc2UgTk9GSV9UWVBFUy5WYW5pbGxhOlxyXG4gICAgICAgIHJldHVybiAnJztcclxuXHJcbiAgICAgIGNhc2UgTk9GSV9UWVBFUy5TdWNjZXNzOlxyXG4gICAgICAgIHJldHVybiAnc3VjY2Vzcyc7XHJcblxyXG4gICAgICBjYXNlIE5PRklfVFlQRVMuSW5mbzpcclxuICAgICAgICByZXR1cm4gJ2luZm8nO1xyXG5cclxuICAgICAgY2FzZSBOT0ZJX1RZUEVTLldhcm5pbmc6XHJcbiAgICAgICAgcmV0dXJuICd3YXJuaW5nJztcclxuXHJcbiAgICAgIGNhc2UgTk9GSV9UWVBFUy5GYWlsdXJlOlxyXG4gICAgICAgIHJldHVybiAnZmFpbHVyZSc7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==