/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NofiComponent } from './nofi.component';
import { NotifierTypePipe } from './pipes/type-class.pipe';
var NofiModule = /** @class */ (function () {
    function NofiModule() {
    }
    NofiModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        NofiComponent,
                        NotifierTypePipe
                    ],
                    imports: [
                        CommonModule,
                        FontAwesomeModule,
                    ],
                    exports: [NofiComponent]
                },] }
    ];
    return NofiModule;
}());
export { NofiModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9maS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ub2ZpLyIsInNvdXJjZXMiOlsibGliL25vZmkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxrQ0FBa0MsQ0FBQztBQUNuRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFHekQ7SUFBQTtJQVlBLENBQUM7O2dCQVpBLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osYUFBYTt3QkFDYixnQkFBZ0I7cUJBQ2pCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLGlCQUFpQjtxQkFDbEI7b0JBQ0QsT0FBTyxFQUFFLENBQUMsYUFBYSxDQUFDO2lCQUN6Qjs7SUFFRCxpQkFBQztDQUFBLEFBWkQsSUFZQztTQURZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtGb250QXdlc29tZU1vZHVsZX0gZnJvbSAnQGZvcnRhd2Vzb21lL2FuZ3VsYXItZm9udGF3ZXNvbWUnO1xuaW1wb3J0IHtOb2ZpQ29tcG9uZW50fSBmcm9tICcuL25vZmkuY29tcG9uZW50JztcbmltcG9ydCB7Tm90aWZpZXJUeXBlUGlwZX0gZnJvbSAnLi9waXBlcy90eXBlLWNsYXNzLnBpcGUnO1xuXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIE5vZmlDb21wb25lbnQsXG4gICAgTm90aWZpZXJUeXBlUGlwZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEZvbnRBd2Vzb21lTW9kdWxlLFxuICBdLFxuICBleHBvcnRzOiBbTm9maUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTm9maU1vZHVsZSB7XG59XG4iXX0=