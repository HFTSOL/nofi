import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {INofi, NOFI_TYPES} from './nofi';

@Injectable({
  providedIn: 'root'
})
export class NofiService {
  private _notifier: Subject<INofi> = null;

  constructor() {
  }

  public notify(notification: INofi) {
    if (this._notifier) {
      this._notifier.next(notification);
    }
  }

  public success(title: string, message: string, duration = 25000) {
    this.notify({
      type: NOFI_TYPES.Success,
      title,
      message,
      duration
    });
  }

  public info(title: string, message: string, duration = 25000) {
    this.notify({
      type: NOFI_TYPES.Info,
      title,
      message,
      duration
    });
  }

  public warning(title: string, message: string, duration = 25000) {
    this.notify({
      type: NOFI_TYPES.Warning,
      title,
      message,
      duration
    });
  }

  public failure(title: string, message: string, duration = 25000) {
    this.notify({
      type: NOFI_TYPES.Failure,
      title,
      message,
      duration
    });
  }

  public register(): Observable<INofi> {
    this._notifier = new Subject<INofi>();
    return this._notifier;
  }
}
