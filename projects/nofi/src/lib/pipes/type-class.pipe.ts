import {Pipe, PipeTransform} from '@angular/core';
import {INofi, NOFI_TYPES} from '../nofi';

@Pipe({name: 'notifierType'})
export class NotifierTypePipe implements PipeTransform {
  transform(nofi: INofi): string {
    switch(nofi.type){
      case NOFI_TYPES.Vanilla:
        return '';

      case NOFI_TYPES.Success:
        return 'success';

      case NOFI_TYPES.Info:
        return 'info';

      case NOFI_TYPES.Warning:
        return 'warning';

      case NOFI_TYPES.Failure:
        return 'failure';
    }
  }
}
