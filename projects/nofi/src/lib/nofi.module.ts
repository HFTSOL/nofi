import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NofiComponent} from './nofi.component';
import {NotifierTypePipe} from './pipes/type-class.pipe';


@NgModule({
  declarations: [
    NofiComponent,
    NotifierTypePipe
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
  ],
  exports: [NofiComponent]
})
export class NofiModule {
}
