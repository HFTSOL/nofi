import { Component, OnInit } from '@angular/core';
import {Guid} from 'guid-typescript';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons';
import {INofi, INofiButton, NOFI_TYPES} from './nofi';

@Component({
  selector: 'lib-nofi',
  templateUrl: './nofi.html',
  styleUrls: ['./nofi.scss']
})
export class NofiComponent implements OnInit {

  public readonly NOFI_TYPES = NOFI_TYPES;

  // Icons
  public faTimesCircle = faTimesCircle;

  public get notifications() {
    return this._notifications;
  }

  private _notifications: INofi[] = [];

  constructor() { }

  ngOnInit() {
  }

  public onCloseClick(nofiId: string) {
    this._timeoutNotification(nofiId);
  }

  public onNofiBtnClick(nofi: INofi, nofiBtn: INofiButton) {
    if (nofi) {
      if (nofiBtn.callback && typeof nofiBtn.callback === 'function')
        nofiBtn.callback();
      else if (nofi.callback && typeof nofi.callback === 'function')
        nofi.callback();

      this._timeoutNotification(nofi.id);
    }
  }

  private _onNotification = (nofi: INofi) => {
    nofi.id = Guid.create().toString();

    if (nofi.duration && !isNaN(nofi.duration))
      setTimeout(this._timeoutNotification.bind(this, nofi.id), nofi.duration);

    this._notifications.push(nofi);
  };

  private _timeoutNotification(nofiId: string) {
    // this._logger.debug(`${this.CLASSNAME} > _timeoutNotification() > nofiId: ${nofiId}`);

    if (!nofiId)
      return;

    const index = this._notifications.findIndex((n: INofi) => {
      return n.id === nofiId;
    });

    if (index >= 0)
      this._notifications.splice(index, 1);
  }
}
