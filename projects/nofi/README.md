# Nofi

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.11.

## Code scaffolding

Run `ng generate component component-name --project nofi` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project nofi`.
> Note: Don't forget to add `--project nofi` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build nofi` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build nofi`, go to the dist folder `cd dist/nofi` and run `npm publish`.

## Running unit tests

Run `ng test nofi` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
